terraform {
  backend "s3" {
    bucket         = "recipie-app-kamil-tf-state"
    key            = "recipe-app.tfstate"
    region         = "eu-west-2"
    encrypt        = true
    dynamodb_table = "recipe-app-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "eu-west-2"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.maintainer
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}