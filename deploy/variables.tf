variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "maintainer" {
  default = "kamilc.dev@gmail.com"
}

variable "db_username" {
  description = "Username for RDS postgress instance"
}

variable "db_password" {
  description = "Password for RDS postgress instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-devops-bastion"
}